# Paymentology CSV

### Application https://paymentology-csv.herokuapp.com/

### Versions
Java version 1.8 (heroku supports only 1.8)
Spring boot version 2.7.8

### Application run command 
```shell
git clone git@gitlab.com:verden/paymentology-csv.git
cd paymentology-csv && ./gradlew clean build
java -jar build/libs/paymentology-csv-0.0.1.jar
```

Default port dev env url http://localhost:8080/

#### Application additional configuration can be set in application.yaml file
```yaml
paymentology:
  matching:
    min-score: 232
    perfect-score: 255 
```
You can control transaction minimum matching score and perfect matching by setting min-score property
and perfect score property.

#### Validation
Added minimum validation in to check file supported types (csv), not null and not empty.
For csv file content, in case if header missing or value of header is wrong type, that data will be ignored and matching 
score will be 0 so there will not match with any of row from destination csv.

#### In this section I will try to describe the analyzed results of CSV files transactions fields (headers) per their importance.
Fields order is presented from most important to less important per priority.
```java

public enum TxPropertyScore {
  WALLET_REFERENCE(128),
  TRANSACTION_ID(64),
  TRANSACTION_TYPE(32),
  TRANSACTION_NARRATIVE(16),
  TRANSACTION_DATE(8),
  TRANSACTION_AMOUNT(4),
  TRANSACTION_DESCRIPTION(2),
  PROFILE_NAME(1)
}
```
TxPropertyScore describes CSV files header importance. The number given after header name is the header weight (score).
The sequence of numbers match power of 2 n geometric progression (1, 2, 4, 8, 16, 32, 64, 128, 256, 512).
Summary of sequential numbers in this progression is always smaller than the next sequence number (1 + 2 + 4  < 8).
This arithmetical model is chosen to avoid priority weights collision.

#### CSV header description
- WALLET_REFERENCE(128) - (the highest score).Based on my analysis I concluded that Wallet Reference ID is the most important property 
because the Wallet Reference ID is unique worldwide.
Wallet Reference ID is of higher priority than Transaction ID because, for example, 
in case if two different banks create transactions with some transaction IDs, 
there is a possibility that two transaction IDs can match.
---
- TRANSACTION_ID(64) -  Transaction IDs are of higher priority than Transaction Types because, for example, 
in a scope of one bank we can have two different transactions with two different Transaction IDs and Wallet Reference IDs
but with the same Transaction Type.
---
- TRANSACTION_TYPE(32) - Transaction Type is of higher importance than Transaction Narrative because, for example,
we can make a payment for our Spotify account 
with two different Transaction Types (POS, TEL, CARD) but Transaction Narrative can be the same.
---
- TRANSACTION_NARRATIVE(16) - Transaction Narrative is of higher importance than Transaction Date because, for example, 
we can have two different transactions
with the same Transaction Date but with two different Transaction Narratives.
---
- TRANSACTION_DATE(8) - Transaction Date is of higher importance than Transaction Amount because, the possibility 
that two different users will do same amount transactions at the exact same time is less 
than the two users will do same amount transactions at different times.
---
- TRANSACTION_AMOUNT(4) - Transaction Amount is of higher importance than Transaction Description because, 
for example, 10 transactions can have the same Transaction Description (Deposit) with different amounts.
---
- TRANSACTION_DESCRIPTION(2) - Transaction Description is of higher importance than Profile Name, because, for example, 
a user can have transactions with different Transaction Descriptions but with the same Profile Name.
---
- PROFILE_NAME(1) - Profile Name is of the lowest importance in this section based on the fact that has no lower comparison field.