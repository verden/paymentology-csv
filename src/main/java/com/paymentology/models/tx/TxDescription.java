package com.paymentology.models.tx;


import java.util.Arrays;

/**
 * @author William Arustamyan
 */


public enum TxDescription {
  DEDUCT, REVERSAL;

  /**
   * Convert String description value to enum
   *
   * @param str string value from csv
   * @return TxDescription type
   */
  public static TxDescription from(final String str) {
    return Arrays.stream(values())
      .filter(it -> it.name().equalsIgnoreCase(str))
      .findFirst()
      .orElse(null);
  }
}
