package com.paymentology.models.tx;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author William Arustamyan
 */


/**
 * Single row of csv file.
 */

@Getter
@Setter
@NoArgsConstructor
public final class Transaction {

  private String profileName;

  private LocalDateTime txDate;

  private BigDecimal txAmount;

  private String txNarrative;

  private TxDescription txDescription;

  private String txId;

  private TxType txType;

  private String walletReference;


  @Override
  public boolean equals(Object o) {
    if (this == o) return true;

    if (o == null || getClass() != o.getClass()) return false;

    Transaction that = (Transaction) o;

    return new EqualsBuilder()
      .append(this.profileName, that.profileName)
      .append(this.txDate, that.txDate)
      .append(this.txAmount, that.txAmount)
      .append(this.txNarrative, that.txNarrative)
      .append(this.txDescription, that.txDescription)
      .append(this.txId, that.txId)
      .append(this.txType, that.txType)
      .append(this.walletReference, that.walletReference)
      .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
      .append(this.profileName)
      .append(this.txDate)
      .append(this.txAmount)
      .append(this.txNarrative)
      .append(this.txDescription)
      .append(this.txId)
      .append(this.txType)
      .append(this.walletReference)
      .toHashCode();
  }
}
