package com.paymentology.models.tx;


import java.util.Arrays;

/**
 * @author William Arustamyan
 */


public enum TxType {
  ZERO, ONE;

  /**
   * Convert String transaction type value to enum
   *
   * @param str string value from csv
   * @return TxType type
   */
  public static TxType from(final String str) {
    return Arrays.stream(values())
      .filter(it -> it.ordinal() == strToInteger(str))
      .findFirst()
      .orElse(null);
  }

  private static Integer strToInteger(final String str) {
    try {
      return Integer.parseInt(str);
    } catch (NumberFormatException ignoreMe) {
      //NOP
    }
    //not supported
    return -1;
  }
}
