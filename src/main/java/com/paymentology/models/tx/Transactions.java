package com.paymentology.models.tx;


import lombok.Getter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * @author William Arustamyan
 */

/**
 * Wrapper class of transactions
 * can contain additional manipulation over transaction list.
 */

@Getter
public final class Transactions implements Iterable<Transaction> {

  private final List<Transaction> transactions = new ArrayList<>();

  public Transactions(final Collection<Transaction> transactions) {
    this.transactions.addAll(transactions);
  }

  @Override
  public Iterator<Transaction> iterator() {
    return this.transactions.listIterator();
  }

  public int size() {
    return this.transactions.size();
  }
}
