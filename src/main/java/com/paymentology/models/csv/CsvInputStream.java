package com.paymentology.models.csv;


import java.io.InputStream;

/**
 * @author William Arustamyan
 */


public final class CsvInputStream {

  public final String name;

  public final InputStream source;

  public CsvInputStream(final String name, final InputStream source) {
    this.name = name;
    this.source = source;
  }
}
