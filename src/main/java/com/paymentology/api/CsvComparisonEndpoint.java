package com.paymentology.api;


import com.paymentology.models.csv.CsvInputStream;
import com.paymentology.services.comparisions.ComparisonService;
import com.paymentology.services.comparisions.TransactionsComparisonResult;
import com.paymentology.services.validation.CsvFileValidationService;
import com.paymentology.services.validation.ValidationResult;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @author William Arustamyan
 */

@Controller
public class CsvComparisonEndpoint {

  private final CsvFileValidationService validationService;
  private final ComparisonService<CsvInputStream> comparisonService;

  public CsvComparisonEndpoint(final ComparisonService<CsvInputStream> comparisonService,
                               final CsvFileValidationService validationService) {
    this.comparisonService = comparisonService;
    this.validationService = validationService;
  }

  @GetMapping("/")
  public String index(Model model) {
    model.addAttribute("showCompareResult", false);
    model.addAttribute("showUnmatchedRecords", false);
    model.addAttribute("showDuplicates", false);
    return "index";
  }


  @PostMapping("/compare")
  public String compare(@RequestParam("leftSource") MultipartFile leftSource, @RequestParam("rightSource") MultipartFile rightSource, Model model) {

    final ValidationResult result = this.validationService.validate(leftSource, rightSource);
    if (result.hasError) {
      model.addAttribute("errorMessage", result.message);
      return "index";
    }

    model.addAttribute("showCompareResult", true);
    model.addAttribute("showUnmatchedRecords", true);

    final CsvInputStream leftStream = this.safeFileNameInputStream(leftSource);
    final CsvInputStream rightStream = this.safeFileNameInputStream(rightSource);

    this.keepFileNames(model, leftStream.name, rightStream.name);

    final TransactionsComparisonResult comparisonResult = this.comparisonService.compare(leftStream, rightStream);
    model.addAttribute("comparisonResult", comparisonResult);
    model.addAttribute("showDuplicates", comparisonResult.hasDuplicates());

    return "index";
  }

  private void keepFileNames(final Model model, final String leftName, final String rightName) {
    model.addAttribute("leftFileName", leftName);
    model.addAttribute("rightFileName", rightName);
  }

  private CsvInputStream safeFileNameInputStream(final MultipartFile mf) {
    try {
      return new CsvInputStream(mf.getOriginalFilename(), mf.getInputStream());
    } catch (final IOException e) {
      throw new RuntimeException("Unable to get input stream from multipart file");
    }
  }
}
