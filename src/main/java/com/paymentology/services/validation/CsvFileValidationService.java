package com.paymentology.services.validation;


import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.Objects;

/**
 * @author William Arustamyan
 */

@Component
public final class CsvFileValidationService {

  private static final String supported_extension = "csv";

  public ValidationResult validate(final MultipartFile leftSource, final MultipartFile rightSource) {
    ValidationResult result = this.validateNullable(leftSource, rightSource);
    if (result.hasError) {
      return result;
    }
    return this.validateFileExtension(leftSource, rightSource);
  }

  private ValidationResult validateNullable(final MultipartFile leftSource, final MultipartFile rightSource) {
    if (leftSource.getSize() == 0 || rightSource.getSize() == 0) {
      return new ValidationResult(true, "Input files are required.");
    }
    return ValidationResult.OK;
  }

  private ValidationResult validateFileExtension(final MultipartFile leftSource, final MultipartFile rightSource) {
    if (!Objects.equals(FilenameUtils.getExtension(leftSource.getOriginalFilename()), supported_extension) ||
      !Objects.equals(FilenameUtils.getExtension(rightSource.getOriginalFilename()), supported_extension)) {
      return new ValidationResult(true, "Input file types not supported");
    }
    return ValidationResult.OK;
  }

}
