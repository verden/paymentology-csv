package com.paymentology.services.validation;


/**
 * @author William Arustamyan
 */


public final class ValidationResult {

  public static final ValidationResult OK = new ValidationResult(false, "");

  public final boolean hasError;

  public final String message;

  public ValidationResult(final boolean hasError, final String message) {
    this.hasError = hasError;
    this.message = message;
  }

}
