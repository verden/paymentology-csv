package com.paymentology.services.matchings;


import com.paymentology.models.tx.Transaction;
import com.paymentology.services.scores.Score;
import com.paymentology.services.scores.ScoreEvaluator;
import com.paymentology.services.scores.TxPropertyScore;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author William Arustamyan
 */

@Service
public final class TransactionsMatchingService implements MatchingService<Collection<Transaction>> {

  private final ScoreEvaluator<Transaction> scoreEvaluator;

  private final Integer minMatchingScore;
  private final Integer perfectMatchingScore;

  public TransactionsMatchingService(final ScoreEvaluator<Transaction> scoreEvaluator,
                                     @Value("${paymentology.matching.min-score}") final Integer minScore,
                                     @Value("${paymentology.matching.perfect-score}") final Integer perfectScore) {
    this.perfectMatchingScore = Optional.ofNullable(perfectScore).orElse(TxPropertyScore.MAX_SCORE);
    this.minMatchingScore = Optional.ofNullable(minScore).orElse(0);
    this.scoreEvaluator = scoreEvaluator;
  }

  @Override
  public MatchedTransactions match(final Collection<Transaction> leftTxs, final Collection<Transaction> rightTxs) {

    final List<MatchedTransaction> pmTxs = new LinkedList<>();
    final List<MatchedTransaction> matchedTxs = new LinkedList<>();

    for (final Transaction leftTx : leftTxs) {
      final Iterator<Transaction> rightIter = rightTxs.iterator();

      while (rightIter.hasNext()) {

        final Transaction rightTx = rightIter.next();
        final Score score = this.scoreEvaluator.evaluate(leftTx, rightTx);
        //check is transactions perfectly matched
        if (Objects.equals(score.score(), this.perfectMatchingScore)) {
          pmTxs.add(new MatchedTransaction(leftTx, rightTx, this.perfectMatchingScore));
          //remove from right side, so we already found perfect match case
          rightIter.remove();
        } else {
          //check is score greater than min allowed score
          if (score.score() > this.minMatchingScore) {
            matchedTxs.add(new MatchedTransaction(leftTx, rightTx, score.score()));
          }
        }
      }
    }
    return new MatchedTransactions(pmTxs, matchedTxs);
  }
}
