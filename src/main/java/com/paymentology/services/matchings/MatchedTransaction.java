package com.paymentology.services.matchings;


import com.paymentology.models.tx.Transaction;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * @author William Arustamyan
 */


public final class MatchedTransaction implements Comparable<MatchedTransaction> {

  public final Transaction left;

  public final Transaction right;

  public final Integer score;

  public MatchedTransaction(final Transaction left, final Transaction right, final Integer score) {
    this._checkNonNegativeScore(score);
    this.left = left;
    this.right = right;
    this.score = score;
  }

  private void _checkNonNegativeScore(final Integer score) {
    if (score == null || score < 0) {
      throw new IllegalArgumentException("Score cannot be null or negative value");
    }
  }

  @Override
  public int compareTo(final MatchedTransaction mt) {
    return this.score.compareTo(mt.score);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;

    if (o == null || getClass() != o.getClass()) return false;

    MatchedTransaction that = (MatchedTransaction) o;

    return new EqualsBuilder()
      .append(left.getTxId(), that.left.getTxId())
      .append(right.getWalletReference(), that.right.getWalletReference())
      .append(score, that.score)
      .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
      .append(left.getTxId())
      .append(right.getWalletReference())
      .append(score)
      .toHashCode();
  }
}
