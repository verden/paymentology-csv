package com.paymentology.services.matchings;


/**
 * @author William Arustamyan
 */


public interface MatchingService<T> {

  MatchedTransactions match(T left, T right);
}
