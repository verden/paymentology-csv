package com.paymentology.services.matchings;


import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * @author William Arustamyan
 */


public final class MatchedTransactions {

  public final List<MatchedTransaction> perfectTxs;

  public final List<MatchedTransaction> matchedTxs;

  public MatchedTransactions(final Collection<MatchedTransaction> perfectTxs,
                             final Collection<MatchedTransaction> matchedTxs) {
    this.perfectTxs = new LinkedList<>(perfectTxs);
    this.matchedTxs = new LinkedList<>(matchedTxs);
  }

  public int matchingCount() {
    return this.perfectTxs.size();
  }

  public int unmatchedCount() {
    return this.matchedTxs.size();
  }
}
