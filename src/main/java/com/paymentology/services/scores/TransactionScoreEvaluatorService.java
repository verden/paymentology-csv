package com.paymentology.services.scores;


import com.paymentology.models.tx.Transaction;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.EnumMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

/**
 * @author William Arustamyan
 */

/**
 * Service to evaluate two transactions matching score
 */

@Service
public final class TransactionScoreEvaluatorService implements ScoreEvaluator<Transaction> {

  private final Map<TxPropertyScore, Function<Transaction, Optional<?>>> propertyFns = new EnumMap<>(TxPropertyScore.class);

  @Override
  public Score evaluate(final Transaction left, final Transaction right) {
    final Score score = new Score();
    TxPropertyScore.properties()
      .forEach(prop -> {
        final Optional<?> opLeft = this.propertyFns.get(prop).apply(left);
        final Optional<?> opRight = this.propertyFns.get(prop).apply(right);
        //check fields is presents (in case if csv file corrupted)
        if (opLeft.isPresent() && opRight.isPresent()) {
          if (opLeft.get().equals(opRight.get())) {
            score.increase(prop.propertyScore());
          }
        }
      });
    return score;
  }

  @PostConstruct
  private void _initPropertyFunctions() {
    this.propertyFns.put(TxPropertyScore.WALLET_REFERENCE, t -> this.wrap(t.getWalletReference()));
    this.propertyFns.put(TxPropertyScore.TRANSACTION_ID, t -> this.wrap(t.getTxId()));
    this.propertyFns.put(TxPropertyScore.TRANSACTION_TYPE, t -> this.wrap(t.getTxType()));
    this.propertyFns.put(TxPropertyScore.TRANSACTION_NARRATIVE, t -> this.wrap(t.getTxNarrative()));
    this.propertyFns.put(TxPropertyScore.TRANSACTION_AMOUNT, t -> this.wrap(t.getTxAmount()));
    this.propertyFns.put(TxPropertyScore.TRANSACTION_DATE, t -> this.wrap(t.getTxDate()));
    this.propertyFns.put(TxPropertyScore.TRANSACTION_DESCRIPTION, t -> this.wrap(t.getTxDescription()));
    this.propertyFns.put(TxPropertyScore.PROFILE_NAME, t -> this.wrap(t.getProfileName()));
  }

  private <T> Optional<T> wrap(T input) {
    return Optional.ofNullable(input);
  }

}
