package com.paymentology.services.scores;


import java.util.Arrays;
import java.util.stream.Stream;

/**
 * @author William Arustamyan
 */


public enum TxPropertyScore {

  WALLET_REFERENCE(128),
  TRANSACTION_ID(64),
  TRANSACTION_TYPE(32),
  TRANSACTION_NARRATIVE(16),
  TRANSACTION_DATE(8),
  TRANSACTION_AMOUNT(4),
  TRANSACTION_DESCRIPTION(2),
  PROFILE_NAME(1);

  public static final Integer MAX_SCORE = TxPropertyScore.properties()
    .map(TxPropertyScore::propertyScore)
    .reduce(0, Integer::sum);
  private final Integer propertyScore;

  TxPropertyScore(final Integer propertyScore) {
    this.propertyScore = propertyScore;
  }

  public static Stream<TxPropertyScore> properties() {
    return Arrays.stream(values());
  }

  public Integer propertyScore() {
    return this.propertyScore;
  }
}
