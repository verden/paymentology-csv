package com.paymentology.services.scores;


/**
 * @author William Arustamyan
 */


public final class Score {

  private Integer score = 0;

  public void increase(final Integer amount) {
    assert amount != null;
    this.score = this.score + amount;
  }

  public Integer score() {
    return this.score;
  }
}
