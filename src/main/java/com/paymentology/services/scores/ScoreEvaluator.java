package com.paymentology.services.scores;


/**
 * @author William Arustamyan
 */


public interface ScoreEvaluator<T> {

  Score evaluate(T left, T right);
}
