package com.paymentology.services.comparisions;


import org.springframework.util.CollectionUtils;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author William Arustamyan
 */


public final class TransactionsComparisonResult {

  public final TxComparisonResult leftResult;

  public final TxComparisonResult rightResult;

  public final List<UnmatchedTxResponse> unmatchedTransactions;

  public final List<DuplicateTransaction> duplicates;

  public TransactionsComparisonResult(final TxComparisonResult leftResult,
                                      final TxComparisonResult rightResult,
                                      final List<UnmatchedTxResponse> unmatchedTransactions) {
    this.leftResult = leftResult;
    this.rightResult = rightResult;
    this.unmatchedTransactions = unmatchedTransactions;

    this.duplicates = new LinkedList<>(this.mapToDuplicate(leftResult.sourceName, leftResult.duplicateTransactions));
    this.duplicates.addAll(this.mapToDuplicate(rightResult.sourceName, leftResult.duplicateTransactions));
  }


  private List<DuplicateTransaction> mapToDuplicate(final String sourceName, final List<TxResponse> txs) {
    return txs.stream()
      .map(tx -> new DuplicateTransaction(sourceName, tx))
      .collect(Collectors.toList());
  }

  public boolean hasDuplicates() {
    return !CollectionUtils.isEmpty(this.duplicates);
  }

  static class DuplicateTransaction {

    public final String sourceName;

    public final TxResponse tx;

    public DuplicateTransaction(final String sourceName, final TxResponse tx) {
      this.sourceName = sourceName;
      this.tx = tx;
    }
  }
}
