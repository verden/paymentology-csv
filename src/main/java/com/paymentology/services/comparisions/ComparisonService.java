package com.paymentology.services.comparisions;


/**
 * @author William Arustamyan
 */


public interface ComparisonService<T> {

  TransactionsComparisonResult compare(T leftSource, T rightSource);

}
