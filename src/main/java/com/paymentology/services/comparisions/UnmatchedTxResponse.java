package com.paymentology.services.comparisions;


import com.paymentology.models.tx.Transaction;

/**
 * @author William Arustamyan
 */


public final class UnmatchedTxResponse {

  public final TxResponse left;

  public final TxResponse right;

  public final Integer score;

  public UnmatchedTxResponse(final Transaction left, final Transaction right, final Integer score) {
    this._checkNonNegativeScore(score);
    this.left = TxResponse.from(left);
    this.right = TxResponse.from(right);
    this.score = score;
  }

  private void _checkNonNegativeScore(final Integer score) {
    if (score == null || score < 0) {
      throw new IllegalArgumentException("Score cannot be null or negative value");
    }
  }
}
