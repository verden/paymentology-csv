package com.paymentology.services.comparisions;


/**
 * @author William Arustamyan
 */

import com.paymentology.models.tx.Transaction;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Simple transaction info, containing some transaction fields
 * (because we don't need to sand back all transaction info)
 */
public final class TxResponse {

  private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

  private final LocalDateTime date;

  private final String reference;

  private final String transactionId;

  private final BigDecimal amount;

  private TxResponse(final LocalDateTime date, final String reference, final String transactionId, final BigDecimal amount) {
    this.date = date;
    this.amount = amount;
    this.reference = reference;
    this.transactionId = transactionId;
  }

  public static TxResponse from(final Transaction transaction) {
    return new TxResponse(
      transaction.getTxDate(),
      transaction.getWalletReference(),
      transaction.getTxId(),
      transaction.getTxAmount()
    );
  }

  public String date() {
    return this.date != null ? this.date.format(dtf) : "";
  }

  public String reference() {
    return this.reference == null ? "" : this.reference;
  }

  public String amount() {
    return this.amount == null ? "" : this.amount.toString();
  }

  public String transactionId() {
    return this.transactionId == null ? "" : this.transactionId;
  }

}
