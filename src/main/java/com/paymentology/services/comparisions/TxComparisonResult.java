package com.paymentology.services.comparisions;


import lombok.Builder;

import java.util.List;

/**
 * @author William Arustamyan
 */


@Builder
public final class TxComparisonResult {

  public final String sourceName;

  public final Integer totalRecords;

  public final Integer duplicateRecords;

  public final Integer matchingRecords;

  public final Integer unmatchedRecords;

  public final List<TxResponse> duplicateTransactions;

}
