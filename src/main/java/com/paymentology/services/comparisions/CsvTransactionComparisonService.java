package com.paymentology.services.comparisions;


import com.paymentology.models.csv.CsvInputStream;
import com.paymentology.models.tx.Transaction;
import com.paymentology.models.tx.Transactions;
import com.paymentology.services.matchings.MatchedTransaction;
import com.paymentology.services.matchings.MatchedTransactions;
import com.paymentology.services.matchings.MatchingService;
import com.paymentology.services.parsers.CsvParser;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author William Arustamyan
 */

@Service
public final class CsvTransactionComparisonService implements ComparisonService<CsvInputStream> {

  private final CsvParser<Transactions> csvParser;
  private final MatchingService<Collection<Transaction>> matchingService;

  public CsvTransactionComparisonService(final CsvParser<Transactions> csvParser,
                                         final MatchingService<Collection<Transaction>> matchingService) {
    this.matchingService = matchingService;
    this.csvParser = csvParser;
  }

  @Override
  public TransactionsComparisonResult compare(final CsvInputStream leftSource, final CsvInputStream rightSource) {
    final Transactions leftTransactions = this.csvParser.parse(leftSource.source);
    final Transactions rightTransactions = this.csvParser.parse(rightSource.source);

    final UniqueDuplicateTxs leftUDT = this.identifyUniqueDuplicateTransactions(leftTransactions);
    final UniqueDuplicateTxs rightUDT = this.identifyUniqueDuplicateTransactions(rightTransactions);
    int rightUniqueCount = leftUDT.uniqueTxs.size();
    final MatchedTransactions mt = this.matchingService.match(leftUDT.uniqueTxs, rightUDT.uniqueTxs);

    final int lUnmatchedCount = leftUDT.uniqueTxs.size() - mt.matchingCount();
    final int rUnmatchedCount = rightUniqueCount - mt.matchingCount();

    return new TransactionsComparisonResult(
      this.buildTxResult(leftSource.name, leftTransactions.size(), this.toTxResponse(leftUDT.duplicateTxs), mt.matchingCount(), lUnmatchedCount),
      this.buildTxResult(rightSource.name, rightTransactions.size(), this.toTxResponse(rightUDT.duplicateTxs), mt.matchingCount(), rUnmatchedCount),
      this.toUnmatchedTxResponse(mt.matchedTxs)
    );
  }

  //sorry for this :(
  private TxComparisonResult buildTxResult(final String sourceName,
                                           int total,
                                           final List<TxResponse> duplicateTxs,
                                           int matchingRecordCount,
                                           int unmatchedRecords) {

    return TxComparisonResult.builder()
      .sourceName(sourceName)
      .totalRecords(total)
      .duplicateRecords(duplicateTxs.size())
      .duplicateTransactions(duplicateTxs)
      .matchingRecords(matchingRecordCount)
      .unmatchedRecords(unmatchedRecords)
      .build();
  }


  private List<TxResponse> toTxResponse(final Collection<Transaction> txs) {
    return txs.stream()
      .map(TxResponse::from)
      .collect(Collectors.toList());
  }

  private List<UnmatchedTxResponse> toUnmatchedTxResponse(final List<MatchedTransaction> transactions) {
    return transactions.stream()
      .map(tx -> new UnmatchedTxResponse(tx.left, tx.right, tx.score))
      .collect(Collectors.toList());
  }


  private UniqueDuplicateTxs identifyUniqueDuplicateTransactions(final Transactions transactions) {
    final UniqueDuplicateTxs udt = new UniqueDuplicateTxs();
    transactions.forEach(udt::addTransaction);
    return udt;
  }

  private static class UniqueDuplicateTxs {

    public final Set<Transaction> duplicateTxs = new LinkedHashSet<>();
    public final Set<Transaction> uniqueTxs = new HashSet<>();

    public void addTransaction(final Transaction tx) {
      if (!this.uniqueTxs.add(tx)) {
        this.duplicateTxs.add(tx);
      }
    }
  }
}
