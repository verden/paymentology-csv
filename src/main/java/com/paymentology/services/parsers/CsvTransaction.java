package com.paymentology.services.parsers;


import com.paymentology.models.tx.Transaction;
import com.paymentology.models.tx.TxDescription;
import com.paymentology.models.tx.TxType;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author William Arustamyan
 */

/**
 * CsvTransaction is functional class to keep {@link Transaction} model separated from csv transaction model
 * contains additional functionality like exception handling and NULL checking.
 */
public final class CsvTransaction {

  private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

  public final Transaction transaction;

  public CsvTransaction() {
    this.transaction = new Transaction();
  }

  public void addProfileName(final String pn) {
    this.transaction.setProfileName(pn);
  }

  public void addTransactionDate(final String strDate) {
    try {
      this.transaction.setTxDate(LocalDateTime.parse(strDate, dtf));
      ;
    } catch (final RuntimeException ignoreMe) {
      //log this
    }
  }

  public void addTransactionAmount(final String strAmount) {
    try {
      this.transaction.setTxAmount(new BigDecimal(strAmount));
    } catch (final RuntimeException ignoreMe) {
      //log this
    }
  }

  public void addTransactionNarrative(final String txNarrative) {
    this.transaction.setTxNarrative(txNarrative);
  }

  public void addTransactionDescription(final String txDsc) {
    this.transaction.setTxDescription(TxDescription.from(txDsc));
  }

  public void addTransactionId(final String txId) {
    this.transaction.setTxId(txId);
  }

  public void addTransactionType(final String strTyp) {
    this.transaction.setTxType(TxType.from(strTyp));
  }

  public void addWalletReference(final String wlRef) {
    this.transaction.setWalletReference(wlRef);
  }
}
