package com.paymentology.services.parsers;


import com.paymentology.models.tx.Transaction;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.EnumMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * @author William Arustamyan
 */


/**
 * Class to transform csv row into {@link Transaction} model.
 */
@Component
final class RowToTxMapper implements Function<CSVRecord, Transaction> {

  private final Map<CsvTxHeader, BiConsumer<CsvTransaction, String>> setterFns = new EnumMap<>(CsvTxHeader.class);

  @Override
  public Transaction apply(final CSVRecord record) {
    final CsvTransaction csvTx = new CsvTransaction();
    CsvTxHeader.items().forEach(
      it -> this.setterFns.get(it).accept(csvTx, this.safeSelect(record, it))
    );
    return csvTx.transaction;
  }

  private String safeSelect(final CSVRecord record, final CsvTxHeader header) {
    try {
      return record.get(header);
    } catch (RuntimeException ignoreMe) {
      //NOP
    }
    return null;
  }

  @PostConstruct
  private void _initSetters() {
    this.setterFns.put(CsvTxHeader.ProfileName, CsvTransaction::addProfileName);
    this.setterFns.put(CsvTxHeader.TransactionID, CsvTransaction::addTransactionId);
    this.setterFns.put(CsvTxHeader.WalletReference, CsvTransaction::addWalletReference);
    this.setterFns.put(CsvTxHeader.TransactionType, CsvTransaction::addTransactionType);
    this.setterFns.put(CsvTxHeader.TransactionDate, CsvTransaction::addTransactionDate);
    this.setterFns.put(CsvTxHeader.TransactionAmount, CsvTransaction::addTransactionAmount);
    this.setterFns.put(CsvTxHeader.TransactionNarrative, CsvTransaction::addTransactionNarrative);
    this.setterFns.put(CsvTxHeader.TransactionDescription, CsvTransaction::addTransactionDescription);
  }
}
