package com.paymentology.services.parsers;


import java.util.Arrays;
import java.util.stream.Stream;

/**
 * @author William Arustamyan
 */

/**
 * Cav file headers
 */
public enum CsvTxHeader {

  ProfileName,
  TransactionDate,
  TransactionAmount,
  TransactionNarrative,
  TransactionDescription,
  TransactionID,
  TransactionType,
  WalletReference;

  public static Stream<CsvTxHeader> items() {
    return Arrays.stream(values());
  }
}
