package com.paymentology.services.parsers;


import com.paymentology.models.tx.Transactions;
import org.apache.commons.csv.CSVParser;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

/**
 * @author William Arustamyan
 */

/**
 * Service to convert csv rows into {@link Transactions} model
 */

@Service
public final class TransactionsCsvParser implements CsvParser<Transactions> {

  private final RowToTxMapper txMapper;

  public TransactionsCsvParser(final RowToTxMapper txMapper) {
    this.txMapper = txMapper;
  }

  @Override
  public Transactions parse(final InputStream source) {
    try (final CSVParser csvParser = this.makeParser(source)) {
      return new Transactions(
        csvParser.stream()
          .map(this.txMapper)
          .collect(Collectors.toList())
      );
    } catch (final IOException e) {
      throw new RuntimeException("Something went wrong: unable to create csv parser", e);
    }
  }

  private CSVParser makeParser(final InputStream source) {
    try {
      return new CSVParser(new InputStreamReader(source), default_format);
    } catch (final IOException e) {
      throw new RuntimeException("Something went wrong: unable to create csv parser", e);
    }
  }
}
