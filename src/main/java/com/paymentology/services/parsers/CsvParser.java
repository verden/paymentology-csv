package com.paymentology.services.parsers;


import org.apache.commons.csv.CSVFormat;

import java.io.InputStream;

/**
 * @author William Arustamyan
 */


public interface CsvParser<T> {

  CSVFormat default_format = CSVFormat.DEFAULT.builder()
    .setTrim(true)
    .setIgnoreHeaderCase(true)
    .setHeader(CsvTxHeader.class)
    .setSkipHeaderRecord(true)
    .build();

  T parse(InputStream source);
}
