package com.paymentology;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class PaymentologyCsvApplication {

  public static void main(String[] args) {
    new SpringApplicationBuilder(PaymentologyCsvApplication.class)
      .bannerMode(Banner.Mode.OFF)
      .run(args);
  }

}
