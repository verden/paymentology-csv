$(document).ready(function () {
  handleFileSelection();
  displayUnmatchedRecords();
  hideUnmatchedRecords();
  displayDuplicateRecords();
  hideDuplicateRecords();
});


function handleFileSelection() {
  $("#leftSource").change(function (e) {
    let leftFileName = e.target.files[0].name;
    if (leftFileName === undefined) {
      $("#leftFileInput").val('');
    } else {
      $("#leftFileInput").val(e.target.files[0].name);
    }
  });

  $("#rightSource").change(function (e) {
    let rightFileName = e.target.files[0].name;
    if (rightFileName === undefined) {
      $("#rightFileInput").val('');
    } else {
      $("#rightFileInput").val(e.target.files[0].name);
    }
  });
}

function displayUnmatchedRecords() {
  $("#unmatched-records-btn").click(function () {
    $("#unmatched-records").show();
  })
}

function hideUnmatchedRecords() {
  $("#unmatched-records-hide").click(function () {
    $("#unmatched-records").hide();
  })
}

function displayDuplicateRecords() {
  $("#duplicate-records-btn").click(function () {
    $("#duplicate-records").show();
  })
}

function hideDuplicateRecords() {
  $("#duplicate-records-hide").click(function () {
    $("#duplicate-records").hide();
  })
}
